//
//  ViewController.swift
//  DwellCMMotion
//
//  Created by Shan-e-Ali Shah on 2/11/17.
//  Copyright © 2017 Shan-e-Ali Shah. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import CoreMotion

class DwellDemoViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate{
    
    //IBOutlets:
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    
    //managers:
    var locationManager = CLLocationManager()
    var motionManager = CMMotionManager()
    var activityManager = CMMotionActivityManager()
    
    //instance variables:
    var activityTimer: Timer?
    var locationUpdates = [CLLocation]() //array for storing significant location change triggered locations
    var initialLocation: CLLocationCoordinate2D!
    var dwellRegion: CLCircularRegion!
    private var machine: StateMachine<DwellDemoViewController>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Set map the map and location manager:
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
        mapView.isZoomEnabled = true
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - locationManager
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion){
        print("Exited dwell region")
        self.machine.state = .Traveling
    }
    
    //MARK: - @IBActions
    @IBAction func startButtonPressed(_ sender: UIButton) {
        //self.startMotionMonitoring()
        self.startActivityMonitoring()
        startButton.isHidden = true
    }    
    
    //MARK: - Helper functions
   /*
    func startMotionMonitoring()
    {
        motionManager.gyroUpdateInterval = 0.2
        motionManager.accelerometerUpdateInterval = 0.2
        motionManager.startAccelerometerUpdates(to: OperationQueue.current!) { (accelerometerData: CMAccelerometerData?, Error) -> Void in
            
            //self.outputAccData(acceleration: accelerometerData!.acceleration)
            
            if (Error != nil)
            {
                print("\(Error)")
            }
        }
        motionManager.startGyroUpdates(to: OperationQueue.current!, withHandler: { (gyroData: CMGyroData?, Error) -> Void in
            
            //self.outputRotData(rotation: gyroData!.rotationRate)
            
            if(Error != nil)
            {
                print("\(Error)")
            }
            
        })
    }
    */
    func startActivityMonitoring()
    {
        activityManager.startActivityUpdates(to: OperationQueue.current!) {(activity: CMMotionActivity?) -> Void in
            if(activity?.stationary)!
            {
                print("activity registered: stationary")
                self.machine.state = .Stationary
            }
            else if (activity?.unknown)!{
                print("activity registered: unknown")
            }
            else {
                if (activity?.walking)!
                {
                    print("activity registered: walking")
                }
                else if (activity?.running)!
                {
                    print("activity registered: running")
                }
                else if (activity?.cycling)!{
                    print("activity registered: cycling")
                }
                else if (activity?.automotive)!
                {
                    print("activity registered: automotive")
                }
                self.machine.state = .Roaming
            }
        }
    }
    
    func stopMotionMonitoring()
    {
        motionManager.stopAccelerometerUpdates()
        motionManager.stopGyroUpdates()
    }
    
    func stopActivityMonitoring()
    {
        activityManager.stopActivityUpdates()
    }
    
    //method to drop pin at location of a visit, is just a temporary place holder for geofence creation method to be implemented later
    func visitPin()
    {
        self.stopActivityTimer()
        print("Geofence should be made now")
        //make geofence
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: self.mapView.userLocation.coordinate.latitude, longitude: self.mapView.userLocation.coordinate.longitude)
        annotation.title = "Geofence"
        self.mapView.addAnnotation(annotation)
        
    }
    
    //MARK: - Visit Timer functions
    
    func startActivityTimer(){
        activityTimer = Timer.scheduledTimer(timeInterval: TimeInterval(30), target: self, selector: #selector(DwellDemoViewController.checkActivity), userInfo: nil, repeats: true)
    }
    
    func checkActivity()
    {
        let secsSinceActivity = VisitTimer.sharedInstance.secsSinceActivity()
        print("\(secsSinceActivity) secs since last trigger")
        if secsSinceActivity >= 40
        {
            self.visitPin()
        }
    }
    
    func stopActivityTimer()
    {
        activityTimer?.invalidate()
        activityTimer = nil
        VisitTimer.sharedInstance.resetActivity()
    }
    
    //MARK: - STATE MACHINE:
    
    enum DwellState{
        case Stationary
        case Roaming
        case Traveling
    }
    
    required init(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)!
        
        let transitions = [
            DwellState.Stationary: [DwellState.Roaming, DwellState.Stationary],
            DwellState.Roaming: [DwellState.Traveling, DwellState.Roaming, DwellState.Stationary],
            DwellState.Traveling: [DwellState.Stationary]
        ]
        machine = StateMachine(initialState: .Stationary, delegate: self, validTransitions: transitions)
    }
    
    func stationaryStateActivities()
    {
        //When theres no timer and no initial location determined
        if(self.activityTimer == nil && self.initialLocation == nil)
        {
            print("Starting stationary state activities")
            
            //start location updates
            locationManager.startUpdatingLocation()
            
            //Start timer
            VisitTimer.sharedInstance.activity()
            self.startActivityTimer()
            print("Starting timer and picking up location")
            initialLocation = locationManager.location!.coordinate
            print("Captured initial location: \(initialLocation)")
            locationManager.stopUpdatingLocation()
        }
    }
    
    func roamingStateActivities()
    {
        
        //When theres no dwell region determined
        if(self.dwellRegion == nil)
        {
            print("Starting roaming state activities")
            
            //start location updates
            locationManager.startUpdatingLocation()
            
            //If region monitoring available
            if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self)
            {
                //region data
                let title = "Dwell"
                let regionRadius = 1.0 //meters
                
                //set up region
                dwellRegion = CLCircularRegion(center: CLLocationCoordinate2D(latitude: initialLocation.latitude, longitude: initialLocation.longitude), radius: regionRadius, identifier: title)
                locationManager.startMonitoring(for: dwellRegion)
            }
            else{
                print("Can't track regions")
            }
        }
    }
    
    func travelingStateActivities()
    {
        print("Starting traveling state activities")
        
        self.stopActivityTimer()
        locationManager.stopUpdatingLocation()
        locationManager.stopMonitoring(for: dwellRegion)
        dwellRegion = nil
        initialLocation = nil
    }
}

extension DwellDemoViewController: StateMachineDelegateProtocol{
    typealias StateType = DwellState
    
    func didTransitionFrom(from: StateType, to:StateType){
        switch to{
        case .Stationary:
            self.stationaryStateActivities()
        case .Roaming:
            self.roamingStateActivities()
        case .Traveling:
            self.travelingStateActivities()
        }
    }
}

