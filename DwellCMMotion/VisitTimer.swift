//
//  VisitTimer.swift
//  SigLocTest
//
//  Created by Shan-e-Ali Shah on 1/23/17.
//  Copyright © 2017 Shan-e-Ali Shah. All rights reserved.
//

import Foundation

class VisitTimer{
    var lastActivity: Date?
    
    class var sharedInstance: VisitTimer{
        struct Static{
            static let instance: VisitTimer = VisitTimer()
        }
        
        return Static.instance
    }
    
    fileprivate init()
    {
        print("DeveloperVals initialized")
    }
    
    func activity()
    {
        lastActivity = Date()
    }
    
    func resetActivity()
    {
        self.lastActivity = nil
    }
    
    func secsSinceActivity() -> Int {
        if self.lastActivity == nil {
            self.activity()
            return 0;
        }
        let elapsedTime = Date().timeIntervalSince(self.lastActivity!)
        return Int(elapsedTime)
    }
}
