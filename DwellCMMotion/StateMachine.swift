//
//  StateMachine.swift
//  DwellCMMotion
//
//  Created by Shan-e-Ali Shah on 2/21/17.
//  Copyright © 2017 Shan-e-Ali Shah. All rights reserved.
//

import Foundation

protocol StateMachineDelegateProtocol: class{
    associatedtype StateType: Hashable
    func didTransitionFrom(from: StateType, to: StateType)
}

class StateMachine<P:StateMachineDelegateProtocol>{
    private let delegate:P
    private let validTransitions: [P.StateType: [P.StateType]]
    
    private var _state:P.StateType{
        didSet{
            delegate.didTransitionFrom(from: oldValue, to: _state)
        }
    }
    
    var state: P.StateType{
        get{
            return _state
        }
        set{
            attemptTransitionTo(to: newValue)
        }
    }
    
    init(initialState: P.StateType, delegate:P, validTransitions: [P.StateType: [P.StateType]]){
        _state = initialState
        self.validTransitions = validTransitions
        self.delegate = delegate
    }
    
    private func attemptTransitionTo(to: P.StateType) {
        if let validNexts = validTransitions[_state]
        {
            if validNexts.contains(to){
                _state = to
            }
            else{
                print("error in attempting transition to \(to)")
            }
        }
    }
    
}

